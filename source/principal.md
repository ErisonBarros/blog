---
title: Erison Rosa de Oliveira Barros
---

## Quick Start

Mestre em Ciências Geodésicas e Tecnologias da Geoinformação pela Universidade Federal de Pernambuco (2011) UFPE.Possui graduação em Engenharia Cartográfica pela Universidade Federal de Pernambuco (2007) UFPE. Formação de Nível Médio Técnico em Edificações pelo CEFET-PE (IFPE), Especialização em Auditoria, Avaliação e Perícias da Engenharia (2014) pelo IPOG. Doutorado em Andamento em Engenharia Civil pela Universidade Federal de Pernambuco, na área de concentração: Transportes e Gestão das Infra-estruturas Urbanas. Atualmente exercer o cargo de Professor Assistente no Departamento de Engenharia Cartográfica (Decart) da Universidade Federal de Pernambuco - UFPE; Foi Inspetor Coordenador do Conselho Regional de Engenharia e Agronomia de Pernambuco na Inspetoria de Carpina-PE (2009-2017). Já foi professor substituto no Departamento de Engenharia Cartográfica da Universidade Federal de Pernambuco - UFPE. Já foi Técnico em Reforma e Desenvolvimento Agrário pelo Instituto Nacional de Colonização e Reforma Agrária INCRA na Superintendência do estado de Pernambuco SR 03(PE). Atuou como Vice Presidente do Comitê de Certificação de Imóveis Rurais da Superintendência Regional de Pernambuco SR03 (PE). Foi Membro da Equipe de Testes e Homologação do Sistema de Gestão Fundiária - SIGEF. Membro do GT Sistema de Certificação Automatizada de Imóveis Rurais. Professor Substituto do Departamento de Engenharia Cartográfica da UFPE.

### Create a new post

``` bash
$ hexo new "My New Post"
```

More info: [Writing](https://hexo.io/docs/writing.html)

### Run server

``` bash
$ hexo server
```

More info: [Server](https://hexo.io/docs/server.html)

### Generate static files

``` bash
$ hexo generate
```

More info: [Generating](https://hexo.io/docs/generating.html)

### Deploy to remote sites

``` bash
$ hexo deploy
```

More info: [Deployment](https://hexo.io/docs/deployment.html)
